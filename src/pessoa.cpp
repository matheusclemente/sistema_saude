#include "pessoa.hpp"
#include <string>

Pessoa::Pessoa(){
  nome = " ";
  sobrenome = " ";
  idade = 0;
  cpf = 0;
}
void Pessoa::setNome(string nome){
  this->nome = nome;
}
string Pessoa::getNome(){
  return nome;
}
void Pessoa::setSobrenome(string sobrenome){
  this->sobrenome = sobrenome;
}
string Pessoa::getSobrenome(){
  return sobrenome;
}
void Pessoa::setIdade(int idade){
  this->idade = idade;
}
int Pessoa::getIdade(){
  return idade;
}
void Pessoa::setCpf(int cpf){
  this->cpf = cpf;
}
int Pessoa::getCpf(){
  return cpf;
}
