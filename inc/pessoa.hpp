#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>

using namespace std;

class Pessoa{
private:
  string nome;
  string sobrenome;
  int idade;
  int cpf;

public:
  Pessoa();
  void setNome(string nome);
  string getNome();
  void setSobrenome(string sobrenome);
  string getSobrenome();
  void setIdade(int idade);
  int getIdade();
  void setCpf(int cpf);
  int getCpf();
};

#endif
