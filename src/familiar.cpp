#include "pessoa.hpp"
#include "paciente.hpp"
#include <string>

void setParentesco(string parentesco){
  this->parentesco = parentesco;
}
string getParentesco(){
  return parentesco;
}
void setPaciente(Paciente familiarInternado){
  this->familiarInternado = familiarInternado;
}
Paciente getPaciente(){
  return familiarInternado;
}
