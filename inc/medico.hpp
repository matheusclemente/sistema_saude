#ifndef MEDICO_HPP
#define MEDICO_HPP
#include "pessoa.hpp"
#include <string>

class Medico : public Pessoa {
private:
  string especializacao;
public:
  void setEspecializacao(string especializacao);
  string getEspecializacao();
};

#endif
