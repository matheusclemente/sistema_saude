#ifndef FAMILIAR_HPP
#define FAMILIAR_HPP
#include "pessoa.hpp"
#include "paciente.hpp"
#include <string>

class Familiar : public Pessoa {
private:
  string parentesco;
  Paciente familiarInternado;
public:
  void setParentesco(string parentesco);
  string getParentesco();
  void setPaciente(Paciente familiarInternado);
  Paciente getPaciente();
};

#endif
