#include "pessoa.hpp"
#include "medico.hpp"
#include <string>

void setDiagnostico(string diagnostico){
  this->diagnostico = diagnostico;
}
string getDiagnostico(){
  return diagnostico;
}
void setMedico(Medico medicoResponsavel){
  this->medicoResponsavel = medicoResponsavel;
}
Medico getMedico(){
  return medicoResponsavel;
}
