#ifndef PACIENTE_HPP
#define PACIENTE_HPP
#include "pessoa.hpp"
#include "medico.hpp"
#include <string>


class Paciente : public Pessoa {
private:
  string diagnostico;
  Medico medicoResponsavel;
public:
  void setDiagnostico(string diagnostico);
  string getDiagnostico();
  void setMedico(Medico medicoResponsavel);
  Medico getMedico();
};

#endif
